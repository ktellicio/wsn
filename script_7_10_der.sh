#!/bin/bash

echo "$(date +"%T") - Shell script has started!"
echo "Simulating..."
echo ""
echo "Runtime | Log | Scenario | Simulation configuration"

## Variables

# log: 			Sets the types of log to be displayed
# nNodes: 		Number of nodes
# random: 		Deployment type
# area: 		Monitored area 							(m2)
# packetSize: 		Size of packets, Lora will apply 9 extra bytes 			(bytes)
# initialEnergy: 	Initial energy value in the end devices 			(J)
# supplyVoltage: 	Voltage of the energy source in the end devices 			(V)
# sendPeriod: 		Interval of time between packets 				(min)
# energyUpdate: 	Sets the interval between measurements to the energy source 	(min)
# logPeriod: 		Interval of time between battery queries over all devices 	(h)
# simTime: 		Simulation time 						(h)

## DER

# S4 = 1000, D1, A1
start_time=`date +%s`
./waf --run "energy --nNodes=1000 --log=der" &> contrib/log/scenario7/derA1F1D1T1S4.txt;
echo "$(expr `date +%s` - $start_time)s | DER | Scenario 7 | A1, F1, D1, T1, S4"

# S4 = 1000, D2, A1
start_time=`date +%s`
./waf --run "energy --nNodes=1000 --random=True --log=der" &> contrib/log/scenario7/derA1F1D2T1S4.txt;
echo "$(expr `date +%s` - $start_time)s | DER | Scenario 7 | A1, F1, D2, T1, S4"

# S4 = 1000, D1, A4
start_time=`date +%s`
./waf --run "energy --nNodes=1000 --area=100000000 --log=der " &> contrib/log/scenario7/derA4F1D1T1S4.txt;
echo "$(expr `date +%s` - $start_time)s | DER | Scenario 7 | A4, F1, D1, T1, S4"

# S4 = 1000, D2, A4
start_time=`date +%s`
./waf --run "energy --nNodes=1000 --area=100000000 --random=True --log=der " &> contrib/log/scenario7/derA4F1D2T1S4.txt;
echo "$(expr `date +%s` - $start_time)s | DER | Scenario 7 | A4, F1, D2, T1, S4"

exit;
