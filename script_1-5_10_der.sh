#!/bin/bash

echo "$(date +"%T") - Shell script has started!"
echo "Simulating..."
echo ""
echo "Runtime | Scenario | Simulation configuration"

## Variables

# log: 			Sets the types of log to be displayed
# nNodes: 		Number of nodes
# random: 		Deployment type
# area: 		Monitored area 							(m2)
# packetSize: 		Size of packets, Lora will apply 9 extra bytes 			(bytes)
# initialEnergy: 	Initial energy value in the end devices 			(J)
# supplyVoltage: 	Voltage of the energy source in the end devices 			(V)
# sendPeriod: 		Interval of time between packets 				(min)
# energyUpdate: 	Sets the interval between measurements to the energy source 	(min)
# logPeriod: 		Interval of time between battery queries over all devices 	(h)
# simTime: 		Simulation time 						(h)

## Scenarios

# A2, F1, D1, T1, S1
start_time=`date +%s`
./waf --run "energy --nNodes=1000 --area=100000 --log=der" &> contrib/log/scenario1/derA2F1D1T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 1 | A2, F1, D1, T1, S1"

exit;

# Monitored area 

# A1, F1, D1, T1, S1
start_time=`date +%s`
./waf --run "energy --log=der" &> contrib/log/scenario1/derA1F1D1T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 1 | A1, F1, D1, T1, S1"

# A2, F1, D1, T1, S1
#start_time=`date +%s`
#./waf --run "energy --nNodes=1000 --area=100000 --log=der" &> contrib/log/scenario1/derA2F1D1T1S1.txt;
#echo "$(expr `date +%s` - $start_time)s | Scenario 1 | A2, F1, D1, T1, S1"

# Frequency of transmission

# A1, F1, D1, T1, S1
cp contrib/log/scenario1/derA1F1D1T1S1.txt contrib/log/scenario2/derA1F1D1T1S1.txt;

# A1, F2, D1, T1, S1
start_time=`date +%s`
./waf --run "energy --sendPeriod=6 --log=der" &> contrib/log/scenario2/derA1F2D1T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 2 | A1, F2, D1, T1, S1"

# A1, F3, D1, T1, S1 
start_time=`date +%s`
./waf --run "energy --sendPeriod=3 --log=der" &> contrib/log/scenario2/derA1F3D1T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 2 | A1, F3, D1, T1, S1 "

# Deployment strategy

# A1, F1, D1, T1, S1
cp contrib/log/scenario1/derA1F1D1T1S1.txt contrib/log/scenario3/derA1F1D1T1S1.txt

# A1, F1, D2, T1, S1
start_time=`date +%s`
./waf --run "energy --random=True --log=der" &> contrib/log/scenario3/derA1F1D2T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 3 | A1, F1, D2, T1, S1"

# Message size

# A1, F1, D1, T1, S1
cp contrib/log/scenario1/derA1F1D1T1S1.txt contrib/log/scenario4/derA1F1D1T1S1.txt

# A1, F1, D1, T2, S1
start_time=`date +%s`
./waf --run "energy --packetSize=64 --log=der" &> contrib/log/scenario4/derA1F1D1T2S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 4 | A1, F1, D1, T2, S1"

# A1, F1, D1, T3, S1
start_time=`date +%s`
./waf --run "energy --packetSize=222 --log=der" &> contrib/log/scenario4/derA1F1D1T3S1.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 4 | A1, F1, D1, T3, S1"

# Density of nodes

# A1, F1, D1, T1, S1
cp contrib/log/scenario1/derA1F1D1T1S1.txt contrib/log/scenario5/derA1F1D1T1S1.txt

# A1, F1, D1, T1, S2
start_time=`date +%s`
./waf --run "energy --nNodes=50 --log=der" &> contrib/log/scenario5/derA1F1D1T1S2.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 5 | A1, F1, D1, T1, S2"

# A1, F1, D1, T1, S3
start_time=`date +%s`
./waf --run "energy --nNodes=100 --log=der" &> contrib/log/scenario5/derA1F1D1T1S3.txt;
echo "$(expr `date +%s` - $start_time)s | Scenario 5 | A1, F1, D1, T1, S3"

exit;
