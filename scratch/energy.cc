/*
 * Simulation
 */

#include "ns3/end-device-lora-phy.h"
#include "ns3/gateway-lora-phy.h"
#include "ns3/class-a-end-device-lorawan-mac.h"
#include "ns3/gateway-lorawan-mac.h"
#include "ns3/simulator.h"
#include "ns3/log.h"
#include "ns3/constant-position-mobility-model.h"
#include "ns3/lora-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/node-container.h"
#include "ns3/position-allocator.h"
#include "ns3/periodic-sender-helper.h"
#include "ns3/command-line.h"
#include "ns3/basic-energy-source-helper.h"
#include "ns3/lora-radio-energy-model-helper.h"
#include "ns3/file-helper.h"
#include "ns3/names.h"
#include <algorithm>
#include <ctime>
#include <string>
#include <fstream>
#include <limits>

#include "ns3/show-progress.h"

 #include "ns3/basic-energy-source.h"

using namespace ns3;
using namespace lorawan;

enum log_type {
  BAT,
  DER,
  LAT
};
log_type hashit (std::string const& log);
void setLogs(log_type log);

Ptr<LoraChannel> createLoraChannel();
MobilityHelper readMobilityFromCsv(std::string filename);
BasicEnergySourceHelper setEnergySource(int32_t initialEnergy, int32_t supplyVoltage, int32_t energyUpdate);
LoraRadioEnergyModelHelper setEnergyConsumption();

void PrintEnergy(EnergySourceContainer& sources);
void EnergyDepletionCallback (EnergySourceContainer& sources);

NS_LOG_COMPONENT_DEFINE ("LoraEnergyModelExample");

// Based on the code provided at 
// https://groups.google.com/g/ns-3-users/c/ML5-fwDqCJc/m/v77_ThHCBAAJ
void PrintEnergy(EnergySourceContainer& sources) {
	EnergySourceContainer::Iterator i;
	double med_Energy;
	int id;
	       
	for (i=sources.Begin(); i != sources.End(); i++) {   
		// med_Energy = (*i)->GetEnergyFraction();
		med_Energy = (*i)->GetRemainingEnergy();
		id = (*i)->GetNode()->GetId();

		NS_LOG_UNCOND(Simulator::Now().GetHours () << "h " << id << " " << med_Energy << "J");
	}
}

int main (int argc, char *argv[])
{

  //Time::SetResolution (Time::H);
  
  /*******************************
  *  Set command line arguments  *
  ********************************/
  
  uint32_t 	simTime = 410;            	// Hours
  //uint32_t 	simTime = 87600;            	// Hours
  int32_t 	initialEnergy = 10;    		// Joules
  //int32_t 	initialEnergy = 31752;    	// Joules

  uint32_t	nNodes = 10;
  bool 		random = false;
  uint32_t	area = 1000;			// 1.000 m^2
  uint32_t 	sendPeriod = 30;           	// Minutes
  uint8_t 	packetSize = 20;          	// Bytes (Lora will apply 9 extra bytes)
  int32_t 	supplyVoltage = 3.6;	        // Volts
  int32_t 	energyUpdate = 1440; 	        // Minutes
  int32_t	logPeriod = 6;            	// Hours
  std::string 	log = "bat";			// Log to be registered, where
  				     		//    "bat" = Battery
  				     		//    "der" = Data Extraction Rate
  
  
  CommandLine cmd;
  cmd.AddValue("nNodes", "Number of nodes", nNodes);
  cmd.AddValue("random", "Deployment type", random);
  cmd.AddValue("area", "Monitored area (m2)", area);
  cmd.AddValue("sendPeriod", "Interval of time between packets (min)", sendPeriod);
  cmd.AddValue("simTime", "Simulation time (h)", simTime);
  cmd.AddValue("packetSize", "Size of packets, Lora will apply 9 extra bytes (bytes)", packetSize);
  cmd.AddValue("initialEnergy", "Initial energy value in the end devices (Joules)", initialEnergy);
  cmd.AddValue("supplyVoltage", "Voltage of the energy source in the end devices (Volts)", supplyVoltage);
  cmd.AddValue("energyUpdate", "Sets the interval between measurements to the energy source (min)", energyUpdate);
  cmd.AddValue("log", "Sets the types of log to be displayed", log);
  cmd.AddValue("logPeriod", "Interval of time between battery queries over all devices (h)", logPeriod);
  cmd.Parse(argc, argv);
 
  setLogs(hashit(log));

  NS_LOG_INFO ("Creating the channel...");
  Ptr<LoraChannel> channel = createLoraChannel();

  NS_LOG_INFO ("Creating the mobility model...");
  MobilityHelper mobility;
  
  NS_LOG_INFO ("Reading csv with nodes positions...");
  
  // Getting csv filename with number of nodes and area size
  std::string path_to_points = "./contrib/deploy/deployment";
  path_to_points += "_";
  if (random) {
    path_to_points += "random";
  } else {
    path_to_points += "grid";
  }
  path_to_points += "_nodes_";
  path_to_points += std::to_string( nNodes );
  path_to_points += "_area_";
  path_to_points += std::to_string( area );
  path_to_points += ".csv";
  
  NS_LOG_INFO ("Reading csv at " + path_to_points);
  mobility = readMobilityFromCsv(path_to_points);


  NS_LOG_INFO ("Setting up helpers...");
  // Create the LoraPhyHelper
  LoraPhyHelper phyHelper = LoraPhyHelper ();
  phyHelper.SetChannel (channel);

  // Create the LorawanMacHelper
  LorawanMacHelper macHelper = LorawanMacHelper ();

  // Create the LoraHelper
  LoraHelper helper = LoraHelper ();

  /************************
  *  Create End Devices  *
  ************************/

  NS_LOG_INFO ("Creating end devices...");

  NodeContainer endDevices;
  endDevices.Create (nNodes - 1);
  mobility.Install (endDevices);

  // Create the LoraNetDevices of the end devices
  phyHelper.SetDeviceType (LoraPhyHelper::ED);
  macHelper.SetDeviceType (LorawanMacHelper::ED_A);
  NetDeviceContainer endDevicesNetDevices = helper.Install (phyHelper, macHelper, endDevices);

  /*********************
   *  Create Gateways  *
   *********************/

  NS_LOG_INFO ("Creating the gateway...");
  NodeContainer gateways;
  gateways.Create (1);

  mobility.Install (gateways);

  // Create a netdevice for each gateway
  phyHelper.SetDeviceType (LoraPhyHelper::GW);
  macHelper.SetDeviceType (LorawanMacHelper::GW);
  helper.Install (phyHelper, macHelper, gateways);

  macHelper.SetSpreadingFactorsUp (endDevices, gateways, channel);

  NS_LOG_INFO ("Creating periodic sender application...");
  PeriodicSenderHelper periodicSenderHelper;
  periodicSenderHelper.SetPeriod (Minutes (sendPeriod));
  periodicSenderHelper.SetPacketSize(packetSize);
  periodicSenderHelper.Install (endDevices);

  NS_LOG_INFO ("Creating energy source...");
  BasicEnergySourceHelper energySource = setEnergySource(initialEnergy, supplyVoltage, energyUpdate);
  NS_LOG_INFO ("Creating energy consumption model...");

  // install energy source and consumption model in the end devices
  EnergySourceContainer sources = energySource.Install (endDevices);
  
  LoraRadioEnergyModelHelper energyConsumption = setEnergyConsumption();
  
  DeviceEnergyModelContainer deviceModels = energyConsumption.Install
      (endDevicesNetDevices, sources);

  // Adds the energy source to the namespace, so it could be traced by the fileHelper
  Names::Add ("/Names/EnergySource", sources.Get (0));
  
  /************************
   * Print nodes position *
   ************************/
  // Adapted from https://groups.google.com/g/ns-3-users/c/o_dh4wN-po0
  //NodeContainer const & n = NodeContainer::GetGlobal ();
  //for (NodeContainer::Iterator i = n.Begin (); i != n.End (); ++i) {
  //    Ptr<Node> node = *i;
  //    std::string name = Names::FindName (node); // Assume that nodes are named, remove this line otherwise
  //    Ptr<MobilityModel> mob = node->GetObject<MobilityModel> ();
  //    if (! mob) {
  //          continue;
  //          std::cout << "Node has no mobility model installed. Skip!\n";
  //    }
  //    Vector pos = mob->GetPosition ();
  //    std::cout << "Node " << name << " is at (" << pos.x << ", " << pos.y << ", " << pos.z << ")\n";
  // }


  /**************
   * Get output *
   **************/
  FileHelper fileHelper;
  fileHelper.ConfigureFile ("battery-level", FileAggregator::SPACE_SEPARATED);
  fileHelper.WriteProbe ("ns3::DoubleProbe", "/Names/EnergySource/RemainingEnergy", "Output");
  
  /***************************
  *  Periodic battery check  *
  **********************
  // Based on the code provided at 
  // https://groups.google.com/g/ns-3-users/c/ML5-fwDqCJc/m/v77_ThHCBAAJ******/
  if (log == "bat") {
      uint32_t logTick = 0;
      for(logTick=0; logTick<=simTime; logTick=logTick+logPeriod){
        Simulator::Schedule(Hours(logTick), &PrintEnergy, sources);
      }
  }

  /****************
  *  Simulation  *
  ****************/
  
  NS_LOG_INFO ("");

  Simulator::Stop (Hours (simTime));
  
  //ShowProgress progress (Seconds (10), std::cerr);

  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}

Ptr<LoraChannel> createLoraChannel() {
  Ptr<LogDistancePropagationLossModel> loss = CreateObject<LogDistancePropagationLossModel> ();
  loss->SetPathLossExponent (3.76);
  loss->SetReference (1, 7.7);

  Ptr<PropagationDelayModel> delay = CreateObject<ConstantSpeedPropagationDelayModel> ();

  Ptr<LoraChannel> channel = CreateObject<LoraChannel> (loss, delay);

  return channel;
}

MobilityHelper readMobilityFromCsv(std::string filename) {
  // Print nodes coordinates header
  std::cout << "node_id, x, y, z" << std::endl;
  int32_t node_id = 0;
  
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
  std::string x, y;
  
  std::ifstream inFile(filename);
  Vector position;

  // Reading position
  while(inFile) {
    std::getline(inFile, x, ',');
    std::getline(inFile, y, ',');
    inFile >> position.z;
    if(inFile) {
      position.x = std::stod(x);
      position.y = std::stod(y);
      allocator->Add(position);
      
      // Print nodes coordinates
      std::cout << node_id << ", " << position.x << ", " << position.y << ", " << position.z << std::endl;	
      node_id = node_id + 1;
    }
    inFile.ignore(std::numeric_limits<int>::max(), '\n');
  }

  mobility.SetPositionAllocator (allocator);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  return mobility;
}

BasicEnergySourceHelper setEnergySource(int32_t initialEnergy, int32_t supplyVoltage, int32_t energyUpdate) {
  BasicEnergySourceHelper energySourceHelper;
  energySourceHelper.Set("BasicEnergySourceInitialEnergyJ", DoubleValue (initialEnergy)); // Energy in J
  energySourceHelper.Set("BasicEnergySupplyVoltageV", DoubleValue (supplyVoltage));
  energySourceHelper.Set("PeriodicEnergyUpdateInterval", TimeValue(Minutes(energyUpdate)));

  return energySourceHelper;
}

/* Sets the energy consumption of the end device */
LoraRadioEnergyModelHelper setEnergyConsumption() {
  LoraRadioEnergyModelHelper radioEnergyHelper;

  radioEnergyHelper.Set("StandbyCurrentA", DoubleValue (0.0014));
  //radioEnergyHelper.Set("TxCurrentA", DoubleValue (0.028));
  radioEnergyHelper.Set("TxCurrentA", DoubleValue (0.5));
  radioEnergyHelper.Set("SleepCurrentA", DoubleValue (0.0000015));
  radioEnergyHelper.Set("RxCurrentA", DoubleValue (0.0112));

  radioEnergyHelper.SetTxCurrentModel("ns3::ConstantLoraTxCurrentModel",
                                       "TxCurrent", DoubleValue (0.028)); 
                        
                                               
  return radioEnergyHelper;
}

log_type hashit (std::string const& log) {
  if (log == "bat") return BAT;
  if (log == "der") return DER;
  if (log == "lat") return LAT;
  
  /* Default */
  return BAT;
}

void setLogs(log_type log) {
  /* Prefixes */
  // LogComponentEnableAll(LOG_PREFIX_FUNC);
  LogComponentEnableAll(LOG_PREFIX_NODE);
  LogComponentEnableAll(LOG_PREFIX_TIME);

  // Enabling NS_LOG_INFO prints
  LogComponentEnable("LoraEnergyModelExample", LOG_LEVEL_ALL);
  
  switch (log) {
    case BAT:
      /* Energy Model */
      LogComponentEnable ("LoraRadioEnergyModel", LOG_LEVEL_INFO);
      NS_LOG_INFO ("Battery log!");
      break;
    case DER:
      /* Applications */     
      LogComponentEnable ("EndDeviceLorawanMac", LOG_LEVEL_INFO);
      LogComponentEnable ("GatewayLorawanMac", LOG_LEVEL_INFO);
      NS_LOG_INFO ("DER log!");
      break;
    case LAT:
      /* Applications */
      //LogComponentEnable("PeriodicSenderHelper", LOG_LEVEL_ALL);
      //LogComponentEnable("PeriodicSender", LOG_LEVEL_ALL);
      LogComponentEnable ("EndDeviceLorawanMac", LOG_LEVEL_INFO);
      LogComponentEnable ("GatewayLorawanMac", LOG_LEVEL_INFO);
      NS_LOG_INFO ("Latency log!");
      break;
    default:
      break;
    }

  //LogComponentEnable("LoraEnergyModelExample", LOG_LEVEL_ALL);

  /* Applications */
  //LogComponentEnable("PeriodicSenderHelper", LOG_LEVEL_ALL);
  //LogComponentEnable("PeriodicSender", LOG_LEVEL_ALL);
  //LogComponentEnable ("OneShotSenderHelper", LOG_LEVEL_ALL);
  //LogComponentEnable ("OneShotSender", LOG_LEVEL_ALL);

  /* Energy Model */
  //LogComponentEnable ("LoraRadioEnergyModel", LOG_LEVEL_INFO);

  //LogComponentEnable ("LorawanMacHeader", LOG_LEVEL_ALL);
  //LogComponentEnable ("LoraFrameHeader", LOG_LEVEL_ALL);
  //LogComponentEnable ("LoraChannel", LOG_LEVEL_INFO);
  //LogComponentEnable ("LoraPhy", LOG_LEVEL_ALL);
  //LogComponentEnable ("EndDeviceLoraPhy", LOG_LEVEL_ALL);
  //LogComponentEnable ("GatewayLoraPhy", LOG_LEVEL_ALL);
  //LogComponentEnable ("LoraInterferenceHelper", LOG_LEVEL_ALL);
  //LogComponentEnable ("LorawanMac", LOG_LEVEL_ALL);
  //LogComponentEnable ("EndDeviceLorawanMac", LOG_LEVEL_INFO);
  //LogComponentEnable ("ClassAEndDeviceLorawanMac", LOG_LEVEL_ALL);
  //LogComponentEnable ("GatewayLorawanMac", LOG_LEVEL_INFO);
  //LogComponentEnable ("LogicalLoraChannelHelper", LOG_LEVEL_ALL);
  //LogComponentEnable ("LogicalLoraChannel", LOG_LEVEL_ALL);
  //LogComponentEnable ("LoraHelper", LOG_LEVEL_ALL);
  //LogComponentEnable ("LoraPhyHelper", LOG_LEVEL_ALL);
  //LogComponentEnable ("LorawanMacHelper", LOG_LEVEL_ALL);
}
