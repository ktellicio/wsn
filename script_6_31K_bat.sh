#!/bin/bash

echo "$(date +"%T") - Shell script has started!"
echo "Simulating..."
echo ""
echo "Runtime | Log | Scenario | Simulation configuration"

## Variables

# log: 			Sets the types of log to be displayed
# nNodes: 		Number of nodes
# random: 		Deployment type
# area: 		Monitored area 							(m2)
# packetSize: 		Size of packets, Lora will apply 9 extra bytes 			(bytes)
# initialEnergy: 	Initial energy value in the end devices 			(J)
# supplyVoltage: 	Voltage of the energy source in the end devices 			(V)
# sendPeriod: 		Interval of time between packets 				(min)
# energyUpdate: 	Sets the interval between measurements to the energy source 	(min)
# logPeriod: 		Interval of time between battery queries over all devices 	(h)
# simTime: 		Simulation time 						(h)


## BAT

# A3 = 25000000, D1
start_time=`date +%s`
./waf --run "energy --initialEnergy=31752 --simTime=87600 --area=25000000" &> contrib/log/scenario6/batA3F1D1T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | BAT | Scenario 6 | A3, F1, D1, T1, S1"

# A3 = 25000000, D2
start_time=`date +%s`
./waf --run "energy --initialEnergy=31752 --simTime=87600 --area=25000000 --random=True" &> contrib/log/scenario6/batA3F1D2T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | BAT | Scenario 6 | A3, F1, D2, T1, S1"

# A4 = 100000000, D1
start_time=`date +%s`
./waf --run "energy --initialEnergy=31752 --simTime=87600 --area=100000000" &> contrib/log/scenario6/batA4F1D1T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | BAT | Scenario 6 | A4, F1, D1, T1, S1"

# A4 = 100000000, D2
start_time=`date +%s`
./waf --run "energy --initialEnergy=31752 --simTime=87600 --area=100000000 --random=True" &> contrib/log/scenario6/batA4F1D2T1S1.txt;
echo "$(expr `date +%s` - $start_time)s | BAT | Scenario 6 | A4, F1, D2, T1, S1"

exit;



