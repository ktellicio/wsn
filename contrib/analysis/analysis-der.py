import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
from time import time
from datetime import datetime

pd.options.mode.chained_assignment = None  # default='warn'
pd.set_option('display.max_columns', 999)

plt.rcParams.update({'font.size': 14})

# "3.8 V 3000mAh Li-Po battery, which is 32832 joules"
# in "Comparison of LoRa and NB-IoT in Terms of Power Consumption"
# https://www.diva-portal.org/smash/get/diva2:1451892/FULLTEXT01.pdf

#BATTERY_BUDGET = 32832 # in Joules
BATTERY_BUDGET = 31752 # in Joules

# %%bash
# cd ..  
# cd .. 
# chmod +x script.sh
# ./script.sh

def get_path_list(log):
    
    log_path_list = []
    
    base_config = [log + 'A1F1D1T1S1']

    n_scenario = 1
    log_list = base_config #+ [log + 'A2F1D1T1S1']
    log_path_list += [str(f"scenario{n_scenario}/" + log) for log in log_list]

    n_scenario = 2
    log_list = base_config + [log + 'A1F2D1T1S1', log + 'A1F3D1T1S1']
    log_path_list += [str(f"scenario{n_scenario}/" + log) for log in log_list]

    n_scenario = 3
    log_list = base_config + [log + 'A1F1D2T1S1']
    log_path_list += [str(f"scenario{n_scenario}/" + log) for log in log_list]

    n_scenario = 4
    log_list = base_config + [log + 'A1F1D1T2S1', log + 'A1F1D1T3S1']
    log_path_list += [str(f"scenario{n_scenario}/" + log) for log in log_list]

    n_scenario = 5
    log_list = base_config + [log + 'A1F1D1T1S2', log + 'A1F1D1T1S3']
    log_path_list += [str(f"scenario{n_scenario}/" + log) for log in log_list]
    
    return log_path_list

# -- Read log file and transform into Dataframe --

# Transform log lines into Pandas DataFrame accoding to the type of 'log'.
# Use the following inputs to load the correspondent 'log':
# 'bat' for battery log, with energy consumption
# 'der' for data extraction rate log

def log_to_frame(log, to_csv=False):

    path = f'../log/{log}'

    time = []
    node = []
    packet = []
    received = []
    columns = ['time', 'node', 'packet']

    with open(f'{path}.txt') as file:
        for line_number, line in enumerate(file):
            if(line[0] != '+'):
                continue

            splitted = line.split(' ')

            if(splitted[1] == '-1'):
                continue

            # Delete '+' and 's' from time
            t = float(splitted[0][1:-1].replace('+', '').replace('s', ''))
            n = int(splitted[1])
            p = int(splitted[2][:-1])

            #print(t, n, p)

            time.append(t)
            node.append(n)
            packet.append(p)

    temp = pd.DataFrame(list(zip(time, node, packet)), columns = columns)
    temp.time = temp.time.astype('float')
    temp.node = temp.node.astype('int')
    temp.packet = temp.packet.astype('int')
    
    # Filling dataframe of packets with sent/received messages list 'temp'
    columns = ['packet', 'sender', 'receiver']
    temp_listed = temp.groupby(['packet']).agg({'node': list}).reset_index()
    print('temp_listed executed!')
    
    #packet = temp_listed['packet']
    #sender = temp_listed['node'].apply(lambda node_list: node_list[0])
    #receiver = temp_listed['node'].apply(lambda node_list: node_list[1])
    df = pd.DataFrame(list(zip(temp_listed['packet'], temp_listed['node'].apply(lambda node_list: node_list[0]), temp_listed['node'].apply(lambda node_list: node_list[1]))), columns = columns)
        
    if(to_csv):
        df.to_csv(f'{path}.csv', mode='w', index=False, header=False)
    
    return df
    
log = 'der'
log_path_list = get_path_list(log)
    
now = datetime.now()
current_time = now.strftime("%H:%M:%S")
print(f"{current_time}")

for log_path in log_path_list:
    scenario_config = log_path[-10:]
    scenario_number = log_path[8]
    
    now = datetime.now()
    
    df = log_to_frame(log_path)
    der = (~df['receiver'].isna()).sum() / df['receiver'].count()
    print(f'Cenário {scenario_number} - {scenario_config} - DER = {der}')
    
    duration = (datetime.now() - now).total_seconds()
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print(f'{current_time} - {int(duration)}s')
